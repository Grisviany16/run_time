import 'package:flutter/material.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/view/employee_view.dart';
import 'package:run_time_project/view/profile_view.dart';
import 'package:run_time_project/view/project_view.dart';
import 'package:run_time_project/view/group_view.dart';
import 'package:run_time_project/view/timer_track_view.dart';

class DashboardView extends StatefulWidget {
  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  int _currrentIndex = 0;
  final List<Widget> _children = [
    TimeTrackView(),
    ProjectView(),
    GroupView(),
    EmployeeView(),
    ProfileView(),
  ];

  void onTappedBar(int index) {
    setState(() {
      _currrentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: _children[_currrentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: onTappedBar,
        currentIndex: _currrentIndex,
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.access_time, color: Colors.white),
            title: new Text(
              "Time Track",
              style: TextStyle(color: Colors.white),
            ),
          ),
          BottomNavigationBarItem(
            icon: new Icon(
              Icons.timeline,
              color: Colors.white,
            ),
            title: new Text("Project", style: TextStyle(color: Colors.white)),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.group, color: Colors.white),
            title: new Text("Group", style: TextStyle(color: Colors.white)),
          ),
          BottomNavigationBarItem(
            icon: new Icon(
              Icons.group,
              color: Colors.white,
            ),
            title: new Text("Employee", style: TextStyle(color: Colors.white)),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.person, color: Colors.white),
            title: new Text("Profile", style: TextStyle(color: Colors.white)),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
