import 'package:flutter/material.dart';
import 'package:run_time_project/model/body/create_time_track.dart';
import 'package:run_time_project/model/module_model.dart';
import 'package:run_time_project/model/modules_model.dart';
import 'package:run_time_project/model/time_track_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/util/util.dart';

import 'dashboard_view.dart';

class TimerCardDetailView extends StatefulWidget {
  CreateTimeTrack createTimeTrack;

  TimerCardDetailView({this.createTimeTrack});

  @override
  _TimerCardDetailViewState createState() => _TimerCardDetailViewState();
}

class _TimerCardDetailViewState extends State<TimerCardDetailView> {
  final provider = Provider();
  List<ModuleModel> moduleModels;
  int selectedModuleModelIndex = 0;
  String timeName;
  String timeDescription;

  Future<ModulesModel> _getModulesByProject() async {
    ModulesModel projectsModel =
        await provider.getModulesByProject(widget.createTimeTrack.module);
    return projectsModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Time Card Detail"),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      body: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _timeNameTextField(),
              _timeDescriptionTextField(),
              _timeStartTextField(),
              _timeEndTextField(),
              _moduleDropDown(),
            ],
          ),
          _submitButton(),
        ],
      ),
    );
  }

  Widget _timeNameTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Time Name',
        ),
        onChanged: (String value) async {
          timeName = value;
        },
      ),
    );
  }

  Widget _timeDescriptionTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Time Description',
        ),
        onChanged: (String value) async {
          timeDescription = value;
        },
      ),
    );
  }

  Widget _timeStartTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        initialValue: convertDateFromMilis(widget.createTimeTrack.timeStart),
        enableInteractiveSelection: true,
        enabled: false,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Time Start',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _timeEndTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        initialValue: convertDateFromMilis(widget.createTimeTrack.timeEnd),
        enableInteractiveSelection: true,
        enabled: false,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Time End',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _submitButton() {
    return Container(
      width: 250.0,
      height: 40,
      child: RaisedButton(
        onPressed: () {
          _createTimeTrack();
        },
        color: Color.fromRGBO(27, 20, 100, 1),
        child: Text('Submit', style: TextStyle(color: Colors.white)),
      ),
    );
  }

  Widget _moduleDropDown() {
    return FutureBuilder<ModulesModel>(
      future: _getModulesByProject(),
      builder: (context, snapshot) {
        if (snapshot.data != null) {
          moduleModels = snapshot.data.moduleModels;
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0),
            child: DropdownButton(
              value: moduleModels[selectedModuleModelIndex],
              hint: Text("Choose project"),
              items: moduleModels.map((ModuleModel value) {
                return DropdownMenuItem(
                  value: value,
                  child: new Row(
                    children: <Widget>[new Text(value.moduleDescription)],
                  ),
                );
              }).toList(),
              onChanged: (ModuleModel value) {
                setState(() {
                  selectedModuleModelIndex = moduleModels.indexOf(value);
                });
              },
            ),
          );
        } else {
          return Text('');
        }
      },
    );
  }

  void _createTimeTrack() {
    provider
        .createTimeTrack(
      CreateTimeTrack(
        timeName,
        timeDescription,
        widget.createTimeTrack.timeStart,
        widget.createTimeTrack.timeEnd,
        moduleModels[selectedModuleModelIndex].sId,
      ),
    )
        .then((value) async {
      TimeTrackModel timeTrackModel = value;
      if (timeTrackModel.statusCode == 200) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => DashboardView()),
            (Route<dynamic> route) => false);
      } else if (timeTrackModel.statusCode == 400) {
        showToast(timeTrackModel.message, context);
      }
    }).catchError((onError) {
      showToast(onError.message, context);
    });
  }
}
