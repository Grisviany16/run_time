import 'package:flutter/material.dart';
import 'package:run_time_project/model/body/change_password.dart';
import 'package:run_time_project/model/fake_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/util/util.dart';

class ChangePasswordView extends StatefulWidget {
  @override
  _ChangePasswordViewState createState() => _ChangePasswordViewState();
}

class _ChangePasswordViewState extends State<ChangePasswordView> {
  String _oldPassword = '';
  String _newPassword = '';
  Provider provider = Provider();

  _changePassword() {
    provider
        .changePassword(ChangePasword(_oldPassword, _newPassword))
        .then((value) async {
      FakeModel fakeModel = value;
      if (fakeModel.statusCode == 200) {
        Navigator.pop(context);
      } else if (fakeModel.statusCode == 400) {
        showToast(fakeModel.message, context);
      }
    }).catchError((onError) {
      showToast(onError.message, context);
    });
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
        title: Text("Change Password"),
        centerTitle: true,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
              margin: EdgeInsets.symmetric(vertical: 40, horizontal: 5),
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                      onChanged: (value) {
                        _oldPassword = value;
                      },
                      style: TextStyle(color: Colors.black),
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                          labelText: "Old Password",
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder())),
                  SizedBox(height: 30),
                  TextFormField(
                      onChanged: (value) {
                        _newPassword = value;
                      },
                      style: TextStyle(color: Colors.black),
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                          labelText: "New Password",
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder())),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    width: 200,
                    child: RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _changePassword();
                        }
                      },
                      child: Text(
                        "Save Changes",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Color.fromRGBO(27, 20, 100, 1),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
