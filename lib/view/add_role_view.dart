import 'package:flutter/material.dart';
import 'package:run_time_project/model/body/add_role.dart';
import 'package:run_time_project/model/fake_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/util/util.dart';

class AddRoleView extends StatefulWidget {
  @override
  _AddRoleViewState createState() => _AddRoleViewState();
}

class _AddRoleViewState extends State<AddRoleView> {
  String _role = '';
  Provider provider = Provider();

  _addRole() {
    provider.addRole(AddRole(roleName: _role)).then((value) async {
      FakeModel fakeModel = value;
      if (fakeModel.statusCode == 200) {
        Navigator.pop(context);
      } else if (fakeModel.statusCode == 400) {
        showToast(fakeModel.message, context);
      }
    }).catchError((onError) {
      showToast(onError.message, context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 80.0),
          Expanded(
              child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    TextFormField(
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          filled: true,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          hintText: 'Role Name',
                          labelStyle: TextStyle(color: Colors.white),
                          fillColor: Colors.white),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Role name can not be empty';
                        }
                        return null;
                      },
                      onChanged: (value) {
                        _role = value;
                      },
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      width: 250.0,
                      height: 40,
                      child: RaisedButton(
                        onPressed: () {
                          _addRole();
                        },
                        color: Color.fromRGBO(27, 20, 100, 1),
                        child: Text(
                          'Create Role',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ))
        ],
      ),
    );
  }
}
