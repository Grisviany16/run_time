import 'package:flutter/material.dart';
import 'package:run_time_project/model/module_model.dart';
import 'package:run_time_project/model/modules_model.dart';
import 'package:run_time_project/model/project_model.dart';
import 'package:run_time_project/network/provider.dart';

class ModuleDetailView extends StatefulWidget {
  ModuleModel moduleModel;

  ModuleDetailView({this.moduleModel});

  @override
  _ModuleDetailViewViewState createState() => _ModuleDetailViewViewState();
}

class _ModuleDetailViewViewState extends State<ModuleDetailView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Module Detail'),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          _moduleNameTextField(),
          _moduleStatusTextField(),
          _modulePriorityTextField(),
          _moduleDescriptionTextField(),
        ],
      ),
    );
  }

  Widget _moduleNameTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.moduleModel.moduleName,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Module Name',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _moduleStatusTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.moduleModel.moduleStatus,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Module Status',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _modulePriorityTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.moduleModel.modulePriority,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Module Priority',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _moduleDescriptionTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.moduleModel.moduleDescription,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Module Description',
        ),
        onChanged: (String value) async {},
      ),
    );
  }
}
