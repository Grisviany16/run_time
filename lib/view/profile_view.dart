import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:run_time_project/model/employee_model.dart';
import 'package:run_time_project/view/change_password_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_view.dart';

class ProfileView extends StatefulWidget {
  EmployeeModel item;
  
  ProfileView({this.item});
  
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 250.0,
            color: Color.fromRGBO(27, 20, 100, 1),
            child: Center(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: NetworkImage(
                      "https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg.jpg"),
                  radius: 50.0,
                ),
                SizedBox(height: 10.0),
                Text(
                 widget.item?.employeeFirstName?? '',
                  style: TextStyle(fontSize: 22.0, color: Colors.white),
                ),
                SizedBox(height: 5.0),
                Text(
                  widget.item?.role?.roleName?? '',
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                ),
                SizedBox(height: 10.0),
              ],
            )),
          ),
          Card(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              clipBehavior: Clip.antiAlias,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 10.0, vertical: 10.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Email",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        Text("ricky@acompany.com"),
                      ],
                    )
                  ],
                ),
              )),
          Card(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              clipBehavior: Clip.antiAlias,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 10.0, vertical: 10.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Birthday",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        Text("7 November 1998"),
                      ],
                    ),
                  ],
                ),
              )),
          Card(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              clipBehavior: Clip.antiAlias,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 10.0, vertical: 10.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Status",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        Text("Full Timer"),
                      ],
                    ),
                  ],
                ),
              )),
          Card(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              clipBehavior: Clip.antiAlias,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 10.0, vertical: 10.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Password",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15.0),
                        ),
                        GestureDetector(
                          onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangePasswordView()),
                          ),
                          child: Text(
                            "Change Password",
                            style:
                                TextStyle(decoration: TextDecoration.underline),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )),
          Card(
            margin: EdgeInsets.symmetric(vertical: 3.0),
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Birthday",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15.0),
                      ),
                      Text("7 November 1998"),
                    ],
                  ),
                ],
              ),
            )),
        Card(
            margin: EdgeInsets.symmetric(vertical: 5.0),
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Workspace",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15.0),
                      ),
                      Text("Asd Company"),
                    ],
                  ),
                ],
              ),
            )),
        Card(
            margin: EdgeInsets.symmetric(vertical: 5.0),
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Status",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15.0),
                      ),
                      Text("Full Timer"),
                    ],
                  ),
                ],
              ),
            )),
        Card(
            margin: EdgeInsets.symmetric(vertical: 5.0),
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Password",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15.0),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangePasswordView()),
                        ),
                        child: Text(
                          "Change Password",
                          style:
                              TextStyle(decoration: TextDecoration.underline),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            )),
        Card(
          margin: EdgeInsets.symmetric(vertical: 3.0),
          clipBehavior: Clip.antiAlias,
          color: Colors.white,
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () async {
                        SharedPreferences sharedPreference =
                            await SharedPreferences.getInstance();
                        await sharedPreference.clear();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginView()));
                      },
                      child: Text(
                        "Logout",
                        style: TextStyle(
                            fontSize: 15.0, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
