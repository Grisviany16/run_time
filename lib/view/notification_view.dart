import 'package:flutter/material.dart';
import 'package:run_time_project/model/notification_model.dart';
import 'package:run_time_project/model/notifications_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/view/notification_detail_view.dart';

class NotificationView extends StatelessWidget {
  final provider = Provider();

  Future<NotificationsModel> _getNotifications() async {
    NotificationsModel notificationModels = await provider.getNotifications();
    return notificationModels;
  }

  @override
  final notificationTime = ['Today', 'Today', 'Yesterday', 'Yesterday'];
  final notifDesc = [
    'Do not Forget to end your work time!',
    'Do not forget to start your work time!',
    'Do not Forget to end your work time!',
    'Do not forget to start your work time!'
  ];
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
        title: Text('Notifications'),
      ),
      body: Container(
        color: Colors.white,
        child: FutureBuilder<NotificationsModel>(
          future: _getNotifications(),
          builder: (context, snapshot) {
            if (snapshot.data != null) {
              NotificationsModel notificationsModel = snapshot.data;
              return ListView.builder(
                shrinkWrap: true,
                itemCount: notificationsModel.notificationModels.length,
                itemBuilder: (context, index) {
                  NotificationModel notificationModel =
                      notificationsModel.notificationModels[index];
                  return Card(
                    color: Colors.white,
                    child: ListTile(
                      title: Text(
                        notificationModel.notificationName,
                        textAlign: TextAlign.left,
                      ),
                      trailing: Icon(Icons.keyboard_arrow_right),
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  NotificationDetail(notificationModel))),
                      subtitle: Text(
                        notificationModel.notificationDescription,
                        textAlign: TextAlign.left,
                      ),
                    ),
                  );
                },
              );
            } else {
              return Container(
                child: Center(
                  child: Text('No data'),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
