import 'package:flutter/material.dart';
import 'package:run_time_project/model/employee_model.dart';
import 'package:run_time_project/model/employees_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/view/employee_detail_view.dart';
import 'package:run_time_project/view/register_employee_view.dart';
import 'package:run_time_project/view/role_view.dart';

class EmployeeView extends StatefulWidget {
  @override
  _EmployeeViewState createState() => _EmployeeViewState();
}

class _EmployeeViewState extends State<EmployeeView> {
  final provider = Provider();

  Future<EmployeesModel> _getEmployees() async {
    EmployeesModel employeesModel = await provider.getEmployees();
    return employeesModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Employee"),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RoleView()),
                ),
                child: Icon(
                  Icons.supervised_user_circle,
                  size: 26.0,
                ),
              )),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return RegisterEmployeeView();
          }));
        },
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              FutureBuilder<EmployeesModel>(
                future: _getEmployees(),
                builder: (context, snapshot) {
                  if (snapshot.data != null) {
                    return DataTable(
                      columns: [
                        _dataTable('Name'),
                        _dataTable('Priority'),
                      ],
                      rows: _generateRow(snapshot.data.employeeModels),
                    );
                  } else {
                    return Container(
                      child: Center(
                        child: Text('No data'),
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  _dataTable(String key) {
    return DataColumn(
      label: Text(
        key,
        style: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.black, fontSize: 12),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  List<DataRow> _generateRow(List<EmployeeModel> employeeModels) {
    List<DataRow> dataRows = List();
    for (int i = 0; i < employeeModels.length; i++) {
      EmployeeModel employeeModel = employeeModels[i];
      dataRows.add(
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                  '${employeeModel.employeeFirstName} ${employeeModel.employeeLastName}'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return EmployeeDetailView(employeeModel: employeeModel);
                }));
              },
            ),
            DataCell(
              Text(employeeModel.employeeEmail),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return EmployeeDetailView(employeeModel: employeeModel);
                }));
              },
            ),
          ],
        ),
      );
    }
    return dataRows;
  }
}
