import 'package:flutter/material.dart';
import 'package:run_time_project/model/body/register_employee.dart';
import 'package:run_time_project/model/fake_model.dart';
import 'package:run_time_project/model/role_model.dart';
import 'package:run_time_project/model/roles_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/util/util.dart';

class RegisterEmployeeView extends StatefulWidget {
  @override
  _RegisterEmployeeViewState createState() => _RegisterEmployeeViewState();
}

class _RegisterEmployeeViewState extends State<RegisterEmployeeView> {
  final provider = Provider();
  RolesModel rolesModel;
  String _roleName;
  String _roleId;
  final _formKey = GlobalKey<FormState>();
  bool _validate = false;
  final _text = TextEditingController();
  String firstName = '';
  String lastName = '';
  String email = '';
  String phoneNumber = '';
  String status = '';
  String password = '';

  @override
  void dispose() {
    _text.dispose();
    super.dispose();
  }

  Future<RolesModel> _getRoles() async {
    RolesModel rolesModel = await provider.getRoles();
    return rolesModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
        title: Text("Register Employee"),
        centerTitle: true,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 5),
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                    style: TextStyle(color: Colors.black),
                    keyboardType: TextInputType.text,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field can not be empty';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                      labelText: "First Name",
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(),
                    ),
                    onChanged: (value) {
                      firstName = value;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    style: TextStyle(color: Colors.black),
                    keyboardType: TextInputType.text,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field can not be empty';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                      labelText: "Last Name",
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(),
                    ),
                    onChanged: (value) {
                      lastName = value;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    style: TextStyle(color: Colors.black),
                    keyboardType: TextInputType.text,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field can not be empty';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                        labelText: "Email",
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder()),
                    onChanged: (value) {
                      email = value;
                    },
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    style: TextStyle(color: Colors.black),
                    keyboardType: TextInputType.number,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field can not be empty';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                      labelText: "Phone Number",
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(),
                    ),
                    onChanged: (value) {
                      phoneNumber = value;
                    },
                  ),
                  SizedBox(height: 20),
                  // TextFormField(
                  //   style: TextStyle(color: Colors.black),
                  //   keyboardType: TextInputType.text,
                  //   validator: (text) {
                  //     if (text == null || text.isEmpty) {
                  //       return 'This field can not be empty';
                  //     }
                  //     return null;
                  //   },
                  //   decoration: InputDecoration(
                  //     contentPadding:
                  //         EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                  //     labelText: "Status",
                  //     labelStyle: TextStyle(color: Colors.black),
                  //     border: OutlineInputBorder(),
                  //   ),
                  //   onChanged: (value) {
                  //     status = value;
                  //   },
                  // ),
                  TextFormField(
                    obscureText: true,
                    style: TextStyle(color: Colors.black),
                    keyboardType: TextInputType.text,
                    validator: (text) {
                      if (text == null || text.isEmpty) {
                        return 'This field can not be empty';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                      labelText: "Password",
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(),
                    ),
                    onChanged: (value) {
                      password = value;
                    },
                  ),
                  FutureBuilder<RolesModel>(
                    future: _getRoles(),
                    builder: (context, snapshot) {
                      if (snapshot.data != null) {
                        rolesModel = snapshot.data;

                        return Container(
                          margin: EdgeInsets.symmetric(vertical: 20),
                          padding: EdgeInsets.symmetric(horizontal: 3),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey)),
                          child: DropdownButton(
                              underline: Container(
                                height: 1.0,
                                decoration: const BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Colors.transparent,
                                            width: 0.0))),
                              ),
                              isExpanded: true,
                              hint: Text(
                                "Select Role",
                                style: TextStyle(color: Colors.black),
                              ),
                              value: _roleName,
                              items: rolesModel.roleModels.map((value) {
                                return DropdownMenuItem(
                                  child: Text(value.roleName),
                                  value: value.roleName,
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  _roleName = value;
                                });
                              }),
                        );
                      } else {
                        return Text('No data');
                      }
                    },
                  ),
                  RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _registerEmployee();
                      }
                    },
                    child: Text(
                      "Register",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Color.fromRGBO(27, 20, 100, 1),
                  )
                ],
              )),
        ),
      ),
    );
  }

  _registerEmployee() {
    for (int i = 0; i < rolesModel.roleModels.length; i++) {
      if (rolesModel.roleModels[i].roleName == _roleName) {
        _roleId = rolesModel.roleModels[i].sId;
      }
    }

    print('leot ${_roleId}');
    provider
        .registerEmployee(RegisterEmployee(
            _roleId, firstName, lastName, email, phoneNumber, status, password))
        .then((value) async {
      FakeModel fakeModel = value;
      if (fakeModel.statusCode == 200) {
        Navigator.pop(context);
      } else if (fakeModel.statusCode == 400) {
        showToast(fakeModel.message, context);
      }
    }).catchError((onError) {
      showToast(onError.message, context);
    });
  }
}
