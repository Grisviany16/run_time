import 'package:flutter/material.dart';
import 'package:run_time_project/model/body/create_group.dart';
import 'package:run_time_project/model/fake_model.dart';
import 'package:run_time_project/model/projects_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/util/util.dart';

class CreateGroupView extends StatefulWidget {
  @override
  _CreateGroupViewState createState() => _CreateGroupViewState();
}

class _CreateGroupViewState extends State<CreateGroupView> {
  final provider = Provider();
  String groupName;
  String groupDescription;

  Future<ProjectsModel> _getProjects() async {
    ProjectsModel projectsModel = await provider.getProjects();
    return projectsModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Group"),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      body: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _groupNameTextField(),
              _groupDescriptionTextField(),
            ],
          ),
          _submitButton(),
        ],
      ),
    );
  }

  Widget _groupNameTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Group Name',
        ),
        onChanged: (String value) async {
          groupName = value;
        },
      ),
    );
  }

  Widget _groupDescriptionTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Group Description',
        ),
        onChanged: (String value) async {
          groupDescription = value;
        },
      ),
    );
  }

  Widget _submitButton() {
    return Container(
      width: 250.0,
      height: 40,
      child: RaisedButton(
        onPressed: () {
          _createModule();
        },
        color: Color.fromRGBO(27, 20, 100, 1),
        child: Text('Submit', style: TextStyle(color: Colors.white)),
      ),
    );
  }

  void _createModule() {
    provider
        .createGroup(
      CreateGroup(
        groupName,
        groupDescription,
      ),
    )
        .then((value) async {
      FakeModel fakeModel = value;
      if (fakeModel.statusCode == 200) {
        Navigator.pop(context);
      } else if (fakeModel.statusCode == 400) {
        showToast(fakeModel.message, context);
      }
    }).catchError((onError) {
      showToast(onError.message, context);
    });
  }
}
