import 'package:flutter/material.dart';
import 'package:run_time_project/model/notification_model.dart';

class NotificationDetail extends StatelessWidget {
  NotificationModel notificationModel;

  NotificationDetail(this.notificationModel);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                width: double.infinity,
                height: 50,
                margin: EdgeInsets.only(top: 15, left: 10),
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Reminder",
                      style:
                          TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                    )
                  ],
                )),
            Card(
              elevation: 5,
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: new Container(
                height: 220,
                width: double.maxFinite,
                padding: new EdgeInsets.symmetric(vertical: 70),
                child: new Column(
                  children: <Widget>[
                    new Text(notificationModel.notificationName,
                        style: TextStyle(fontSize: 20)),
                    SizedBox(height: 10),
                    new Text(notificationModel.notificationDescription,
                        style: TextStyle(fontSize: 20))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
