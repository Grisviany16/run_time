import 'package:division/division.dart';
import 'package:flutter/material.dart';

class ListProjectItemView extends StatelessWidget {
  ListProjectItemView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Parent(
        gesture: Gestures()..onTap(() {}),
        style: ParentStyle()
          ..ripple(true)
          ..padding(horizontal: 24)
          ..margin(top: 12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: [
                    Text('item.projectName'),
                    Text('item.projectType'),
                    Text('item.projectTotalHours'),
                  ],
                )
              ],
            ))
          ],
        ),
      ),
    );
  }
}
