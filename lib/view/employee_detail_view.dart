import 'package:flutter/material.dart';
import 'package:run_time_project/model/employee_model.dart';

class EmployeeDetailView extends StatefulWidget {
  EmployeeModel employeeModel;

  EmployeeDetailView({this.employeeModel});

  @override
  _EmployeeDetailViewViewState createState() => _EmployeeDetailViewViewState();
}

class _EmployeeDetailViewViewState extends State<EmployeeDetailView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Group Detail'),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          _firstNameTextField(),
          _lastNameTextField(),
          _emailTextField(),
          _phoneNumberTextField(),
          _roleTextField(),
        ],
      ),
    );
  }

  Widget _firstNameTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.employeeModel.employeeFirstName,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Name',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _lastNameTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.employeeModel.employeeLastName,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Last Name',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _emailTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.employeeModel.employeeEmail,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Email',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _phoneNumberTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.employeeModel.employeePhoneNumber,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Phone Number',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _roleTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.employeeModel.role.roleName,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Role',
        ),
        onChanged: (String value) async {},
      ),
    );
  }
}
