import 'package:flutter/material.dart';
import 'package:run_time_project/model/module_model.dart';
import 'package:run_time_project/model/modules_model.dart';
import 'package:run_time_project/model/project_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/view/create_module_view.dart';
import 'package:run_time_project/view/module_detail_view.dart';

class ModuleView extends StatefulWidget {
  ProjectModel projectModel;

  ModuleView({this.projectModel});

  @override
  _ModuleViewState createState() => _ModuleViewState();
}

class _ModuleViewState extends State<ModuleView> {
  final provider = Provider();

  List<DataRow> _generateRow(List<ModuleModel> moduleModels) {
    List<DataRow> dataRows = List();

    for (int i = 0; i < moduleModels.length; i++) {
      ModuleModel moduleModel = moduleModels[i];

      dataRows.add(DataRow(selected: i % 2 == 0, cells: [
        DataCell(GestureDetector(
          child: Text(moduleModel.moduleName),
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ModuleDetailView(moduleModel: moduleModel);
            }));
          },
        )),
        DataCell(GestureDetector(
          child: Text(moduleModel.moduleDescription),
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ModuleDetailView(moduleModel: moduleModel);
            }));
          },
        )),
        DataCell(GestureDetector(
          child: Text(moduleModel.modulePriority),
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ModuleDetailView(moduleModel: moduleModel);
            }));
          },
        )),
        DataCell(GestureDetector(
          child: Text(moduleModel.moduleStatus),
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ModuleDetailView(moduleModel: moduleModel);
            }));
          },
        )),
      ]));
    }
    return dataRows;
  }

  Future<ModulesModel> _getModulesByProject() async {
    ModulesModel projectsModel =
        await provider.getModulesByProject(widget.projectModel.sId);
    return projectsModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Module'),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
        onPressed: () async {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return CreateModuleView(projectModel: widget.projectModel);
          }));
        },
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: FutureBuilder<ModulesModel>(
          future: _getModulesByProject(),
          builder: (context, snapshot) {
            if (snapshot.data != null) {
              return DataTable(
                  columns: [
                    DataColumn(
                        label: Flexible(
                            child: Text(
                      'Name',
                      textAlign: TextAlign.center,
                    ))),
                    DataColumn(label: Flexible(child: Text('Description'))),
                    DataColumn(label: Text('Priority')),
                    DataColumn(label: Text('Status')),
                  ],
                  columnSpacing: 20,
                  rows: _generateRow(snapshot.data.moduleModels));
            } else {
              return Container(
                child: Center(
                  child: Text('No data'),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
