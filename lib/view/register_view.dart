import 'package:flutter/material.dart';

class RegisterEmployee extends StatefulWidget {
  @override
  _RegisterEmployeeState createState() => _RegisterEmployeeState();
}

class _RegisterEmployeeState extends State<RegisterEmployee> {
  String _valRole;
  List _role = ['Developer', 'Designer', 'Project Manager', 'Human Resource'];
  final _formKey = GlobalKey<FormState>();
  bool _validate = false;
  final _text = TextEditingController();
  @override
  void dispose() {
    _text.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
        title: Text("Register Employee"),
        centerTitle: true,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 5),
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  TextFormField(
                      style: TextStyle(color: Colors.black),
                      keyboardType: TextInputType.text,
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'This field can not be empty';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                          labelText: "First Name",
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder())),
                  SizedBox(height: 20),
                  TextFormField(
                      style: TextStyle(color: Colors.black),
                      keyboardType: TextInputType.text,
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'This field can not be empty';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                          labelText: "Last Name",
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder())),
                  SizedBox(height: 20),
                  TextFormField(
                      style: TextStyle(color: Colors.black),
                      keyboardType: TextInputType.text,
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'This field can not be empty';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                          labelText: "Email",
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder())),
                  SizedBox(height: 20),
                  TextFormField(
                      style: TextStyle(color: Colors.black),
                      keyboardType: TextInputType.number,
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'This field can not be empty';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                          labelText: "Phone Number",
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder())),
                  SizedBox(height: 20),
                  // TextFormField(
                  //     style: TextStyle(color: Colors.black),
                  //     keyboardType: TextInputType.text,
                  //     validator: (text) {
                  //       if (text == null || text.isEmpty) {
                  //         return 'This field can not be empty';
                  //       }
                  //       return null;
                  //     },
                  //     decoration: InputDecoration(
                  //         contentPadding:
                  //             EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                  //         labelText: "Status",
                  //         labelStyle: TextStyle(color: Colors.black),
                  //         border: OutlineInputBorder())),
                  // SizedBox(height: 20),
                  TextFormField(
                      obscureText: true,
                      style: TextStyle(color: Colors.black),
                      keyboardType: TextInputType.text,
                      validator: (text) {
                        if (text == null || text.isEmpty) {
                          return 'This field can not be empty';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                          labelText: "Password",
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder())),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    padding: EdgeInsets.symmetric(horizontal: 3),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey)
                    ),
                    child: DropdownButton(
                        underline: Container(
                          height: 1.0,
                      decoration: const BoxDecoration(
                          border: Border(bottom: BorderSide(color: Colors.transparent, width: 0.0))
                      ),
                        ),
                        isExpanded: true,
                        hint: Text("Select Role", style: TextStyle(color: Colors.black),),
                        value: _valRole,
                        items: _role.map((value) {
                          return DropdownMenuItem(
                            child: Text(value),
                            value: value,
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            _valRole = value;
                          });
                        }),
                  ),
                  RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        // TODO submit
                      }
                    },
                    child: Text(
                      "Register",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Color.fromRGBO(27, 20, 100, 1),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
