import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:run_time_project/model/body/login.dart';
import 'package:run_time_project/model/login_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/util/util.dart';
import 'package:run_time_project/view/dashboard_view.dart';
import 'package:run_time_project/view/forgot_password_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginView extends StatefulWidget {
  LoginView({Key key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final provider = Provider();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      body: Column(
        children: <Widget>[
          SizedBox(height: 155.0),
          Image(image: AssetImage("images/run_time_logo.png")),
          Expanded(
              child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        cursorColor: Colors.white,
                        controller: emailController,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            hintText: 'Email',
                            labelStyle: TextStyle(color: Colors.white),
                            fillColor: Colors.white),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Email can not be empty';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 20.0),
                      TextFormField(
                        obscureText: true,
                        controller: passwordController,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            hintText: 'Password',
                            labelStyle: TextStyle(color: Colors.white),
                            fillColor: Colors.white),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Password can not be empty';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        width: 250.0,
                        height: 40,
                        child: RaisedButton(
                          onPressed: () {
                            _login(emailController.text.toString(),
                                passwordController.text.toString());
                          },
                          color: Colors.blueAccent,
                          child: Text('Login'),
                        ),
                      ),
                      FlatButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ForgotPasswordView()));
                          },
                          child: Text('Forgot Password?',
                              style: TextStyle(color: Colors.white)))
                    ],
                  ),
                ),
              )
            ],
          ))
        ],
      ),
    );
  }

  _login(String email, String password) {
    provider.login(Login(email, password).toJson()).then((value) async {
      LoginModel loginModel = value;
      if (loginModel.statusCode == 200) {
        _saveToken(loginModel.token);
        _saveUser(json.encode(loginModel.employee.toJson()));

        Navigator.push(
            context, MaterialPageRoute(builder: (context) => DashboardView()));
      } else if (loginModel.statusCode == 400) {
        showToast(loginModel.message, context);
      }
    }).catchError((onError) {
      showToast(onError.message, context);
    });
  }

  _saveToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

  _saveUser(String userJson) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('userJson', userJson);
  }
}
