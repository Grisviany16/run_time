import 'package:flutter/material.dart';
import 'package:run_time_project/model/fake_model.dart';
import 'package:run_time_project/model/body/assign_employee_to_group.dart';
import 'package:run_time_project/model/employee_model.dart';
import 'package:run_time_project/model/employees_model.dart';
import 'package:run_time_project/model/group_model.dart';
import 'package:run_time_project/model/login_model.dart';
import 'package:run_time_project/model/module_model.dart';
import 'package:run_time_project/model/modules_model.dart';
import 'package:run_time_project/model/project_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/util/util.dart';

class AssignEmployeeToGroupView extends StatefulWidget {
  GroupModel groupModel;

  AssignEmployeeToGroupView({this.groupModel});

  @override
  _AssignEmployeeToGroupViewViewState createState() =>
      _AssignEmployeeToGroupViewViewState();
}

enum Leader { yes, no }

class _AssignEmployeeToGroupViewViewState
    extends State<AssignEmployeeToGroupView> {
  final provider = Provider();
  List<EmployeeModel> employeeModels;
  int selectedEmployeeModelIndex = 0;
  Leader _leader = Leader.yes;

  Future<EmployeesModel> _getEmployees() async {
    EmployeesModel employeesModel = await provider.getEmployees();
    return employeesModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Group Detail'),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _moduleDropDown(),
              _radioButton(),
            ],
          ),
          _submitButton(),
        ],
      ),
    );
  }

  Widget _moduleDropDown() {
    return FutureBuilder<EmployeesModel>(
      future: _getEmployees(),
      builder: (context, snapshot) {
        if (snapshot.data != null) {
          employeeModels = snapshot.data.employeeModels;
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 30.0),
            child: DropdownButton(
              value: employeeModels[selectedEmployeeModelIndex],
              hint: Text("Choose project"),
              items: employeeModels.map((EmployeeModel value) {
                return DropdownMenuItem(
                  value: value,
                  child: new Row(
                    children: <Widget>[new Text(value.employeeFirstName)],
                  ),
                );
              }).toList(),
              onChanged: (EmployeeModel value) {
                setState(() {
                  selectedEmployeeModelIndex = employeeModels.indexOf(value);
                });
              },
            ),
          );
        } else {
          return Text('');
        }
      },
    );
  }

  Widget _radioButton() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
          child: Text(
            "Leader",
            style: TextStyle(fontWeight: FontWeight.w700),
          ),
        ),
        ListTile(
          title: const Text('Yes'),
          leading: Radio(
            value: Leader.yes,
            groupValue: _leader,
            onChanged: (Leader value) {
              setState(() {
                _leader = value;
              });
            },
          ),
        ),
        ListTile(
          title: const Text('No'),
          leading: Radio(
            value: Leader.no,
            groupValue: _leader,
            onChanged: (Leader value) {
              setState(() {
                _leader = value;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget _submitButton() {
    return Container(
      width: 250.0,
      height: 40,
      child: RaisedButton(
        onPressed: () {
          _assign();
        },
        color: Color.fromRGBO(27, 20, 100, 1),
        child: Text(
          'Assign employee to group',
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  _assign() {
    String groupId = widget.groupModel.sId;
    String employeeId = employeeModels[selectedEmployeeModelIndex].sId;
    bool isLeader = _leader == Leader.yes ? true : false;
    provider
        .assignEmployeeToGroup(
            AssignEmployeeToGroup(groupId, employeeId, isLeader))
        .then((value) async {
      FakeModel fakeModel = value;
      if (fakeModel.statusCode == 200) {
        Navigator.pop(context);
      } else if (fakeModel.statusCode == 400) {
        showToast(fakeModel.message, context);
      }
    }).catchError((onError) {
      showToast(onError.message, context);
    });
  }
}
