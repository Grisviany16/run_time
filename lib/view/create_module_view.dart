import 'package:flutter/material.dart';
import 'package:run_time_project/model/body/create_module.dart';
import 'package:run_time_project/model/body/create_time_track.dart';
import 'package:run_time_project/model/fake_model.dart';
import 'package:run_time_project/model/module_model.dart';
import 'package:run_time_project/model/modules_model.dart';
import 'package:run_time_project/model/project_model.dart';
import 'package:run_time_project/model/projects_model.dart';
import 'package:run_time_project/model/time_track_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/util/util.dart';

import 'dashboard_view.dart';

class CreateModuleView extends StatefulWidget {
  ProjectModel projectModel;
  CreateModuleView({this.projectModel});

  @override
  _CreateModuleViewState createState() => _CreateModuleViewState();
}

class _CreateModuleViewState extends State<CreateModuleView> {
  final provider = Provider();
  String _valPriority;
  List _priority = ['high', 'normal', 'low'];
  int selectedProjectModelIndex = 0;
  List<ProjectModel> projectModels;
  String moduleName = '';
  String moduleDescription = '';
  String moduleStatus = '';
  String project = '';

  Future<ProjectsModel> _getProjects() async {
    ProjectsModel projectsModel = await provider.getProjects();
    return projectsModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Module"),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      body: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
//              _projectsDropDown(),
              _moduleNameTextField(),
//              _moduleStatusTextField(),
              _moduleDescriptionTextField(),
              _modulePriorityDropDown(),
            ],
          ),
          _submitButton(),
        ],
      ),
    );
  }

  Widget _projectsDropDown() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      child: FutureBuilder<ProjectsModel>(
        future: _getProjects(),
        builder: (context, snapshot) {
          if (snapshot.data != null) {
            projectModels = snapshot.data.projectModels;
            if (projectModels.length != 0) {
              return DropdownButton(
                value: projectModels[selectedProjectModelIndex],
                hint: Text("Choose project"),
                items: projectModels.map((ProjectModel value) {
                  return DropdownMenuItem(
                    value: value,
                    child: new Row(
                      children: <Widget>[new Text(value.projectName)],
                    ),
                  );
                }).toList(),
                onChanged: (ProjectModel value) {
                  setState(() {
                    selectedProjectModelIndex = projectModels.indexOf(value);
                  });
                },
              );
            } else {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('No data'),
              );
            }
          } else {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('No data'),
            );
          }
        },
      ),
    );
  }

  Widget _moduleNameTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Module Name',
        ),
        onChanged: (String value) async {
          moduleName = value;
        },
      ),
    );
  }

  Widget _moduleStatusTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Module Status',
        ),
        onChanged: (String value) async {
          moduleStatus = value;
        },
      ),
    );
  }

  Widget _modulePriorityDropDown() {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: DropdownButton(
          underline: Container(
            height: 1.0,
            decoration: const BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.transparent, width: 0.0))),
          ),
          isExpanded: true,
          hint: Text(
            "Select Priority",
            style: TextStyle(color: Colors.black),
          ),
          value: _valPriority,
          items: _priority.map((value) {
            return DropdownMenuItem(
              child: Text(value),
              value: value,
            );
          }).toList(),
          onChanged: (value) {
            setState(() {
              _valPriority = value;
            });
          }),
    );
  }

  Widget _moduleDescriptionTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Module Description',
        ),
        onChanged: (String value) async {
          moduleDescription = value;
        },
      ),
    );
  }

  Widget _submitButton() {
    return Container(
      width: 250.0,
      height: 40,
      child: RaisedButton(
        onPressed: () {
          _createModule();
        },
        color: Color.fromRGBO(27, 20, 100, 1),
        child: Text('Submit', style: TextStyle(color: Colors.white)),
      ),
    );
  }

  void _createModule() {
    if (moduleName.isEmpty) {
      showToast('Fill module name', context);
      return;
    }
    if (moduleDescription.isEmpty) {
      showToast('Fill module descroption', context);
      return;
    }
    if (_valPriority == null) {
      showToast('Select Priority', context);
      return;
    }

    provider
        .createModule(
      CreateModule(
        widget.projectModel.sId,
        moduleName,
        moduleDescription,
        _valPriority,
      ),
    )
        .then((value) async {
      FakeModel fakeModel = value;
      if (fakeModel.statusCode == 200) {
        Navigator.pop(context);
      } else if (fakeModel.statusCode == 400) {
        showToast(fakeModel.message, context);
      }
    }).catchError((onError) {
      showToast(onError.message, context);
    });
  }
}
