import 'package:flutter/material.dart';
import 'package:pdf/pdf.dart';
import 'package:run_time_project/model/group_model.dart';
import 'package:run_time_project/model/groups_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/view/create_group_view.dart';
import 'package:run_time_project/view/group_detail_view.dart';

class GroupView extends StatefulWidget {
  @override
  _GroupViewState createState() => _GroupViewState();
}

class _GroupViewState extends State<GroupView> {
  final provider = Provider();

  Future<GroupsModel> _getGroups() async {
    GroupsModel projectsModel = await provider.getGroups();
    return projectsModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Group"),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return CreateGroupView();
          }));
        },
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: FutureBuilder<GroupsModel>(
            future: _getGroups(),
            builder: (context, snapshot) {
              if (snapshot.data != null) {
                return DataTable(
                  columns: [
                    _dataTable('Name'),
                  ],
                  rows: _generateRow(snapshot.data.groupModels),
                );
              } else {
                return Container(
                  child: Center(
                    child: Text('No data'),
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  _dataTable(String key) {
    return DataColumn(
      label: Text(
        key,
        style: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.black, fontSize: 12),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  List<DataRow> _generateRow(List<GroupModel> groupModels) {
    List<DataRow> dataRows = List();
    for (int i = 0; i < groupModels.length; i++) {
      GroupModel groupModel = groupModels[i];
      dataRows.add(
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(groupModel.groupName),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return GroupDetailView(groupModel: groupModel);
                }));
              },
            ),
          ],
        ),
      );
    }
    return dataRows;
  }
}
