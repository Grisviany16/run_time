import 'package:flutter/material.dart';
import 'package:run_time_project/model/project_model.dart';
import 'package:run_time_project/model/projects_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/view/project_detail_view.dart';

class ProjectView extends StatefulWidget {
  @override
  _ProjectViewState createState() => _ProjectViewState();
}

class _ProjectViewState extends State<ProjectView> {
  final provider = Provider();

  Future<ProjectsModel> _getProjects() async {
    ProjectsModel projectsModel = await provider.getProjects();
    return projectsModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Project"),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              FutureBuilder<ProjectsModel>(
                future: _getProjects(),
                builder: (context, snapshot) {
                  if (snapshot.data != null) {
                    return DataTable(
                      columns: [
                        _dataTable('Project Name'),
                        _dataTable('Type'),
                        _dataTable('Total Hours'),
                      ],
                      rows: _generateRow(snapshot.data.projectModels),
                    );
                  } else {
                    return Container(
                      child: Center(
                        child: Text('No data'),
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  _dataTable(String key) {
    return DataColumn(
      label: Text(
        key,
        style: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.black, fontSize: 12),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  List<DataRow> _generateRow(List<ProjectModel> projectModels) {
    List<DataRow> dataRows = List();
    for (int i = 0; i < projectModels.length; i++) {
      ProjectModel projectModel = projectModels[i];
      dataRows.add(
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(projectModel.projectName),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return ProjectDetailView(projectModel: projectModel);
                }));
              },
            ),
            DataCell(
              Text(projectModel.projectType[0]),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return ProjectDetailView(projectModel: projectModel);
                }));
              },
            ),
            DataCell(
              Text('${projectModel.projectTotalHours}h'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return ProjectDetailView(projectModel: projectModel);
                }));
              },
            ),
          ],
        ),
      );
    }
    return dataRows;
  }
}
