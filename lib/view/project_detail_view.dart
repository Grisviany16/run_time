import 'package:flutter/material.dart';
import 'package:run_time_project/model/project_model.dart';
import 'package:run_time_project/view/module_view.dart';

class ProjectDetailView extends StatefulWidget {
  ProjectModel projectModel;

  ProjectDetailView({this.projectModel});

  @override
  _ProjectDetailViewState createState() => _ProjectDetailViewState();
}

class _ProjectDetailViewState extends State<ProjectDetailView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
          title: new Text("Project Detail"),
          backgroundColor: Color.fromRGBO(27, 20, 100, 1)),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Card(
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Timersheet',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20.0)),
                      Text(widget.projectModel.projectDescription),
                    ]),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 5.0),
                            width: 150,
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border(
                                top: BorderSide(width: 1),
                                left: BorderSide(width: 1),
                                right: BorderSide(width: 1),
                              ),
                              color: Colors.amber,
                            ),
                            child: Text(
                              'Total',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ),
                          Container(
                            width: 150,
                            padding: EdgeInsets.symmetric(vertical: 5.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border(
                                bottom: BorderSide(width: 1),
                                left: BorderSide(width: 1),
                                right: BorderSide(width: 1),
                              ),
                            ),
                            child: Text(
                              '${widget.projectModel.projectTotalHours}h',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20.0),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 30.0),
                      Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 5.0),
                            width: 150,
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border(
                                top: BorderSide(width: 1),
                                left: BorderSide(width: 1),
                                right: BorderSide(width: 1),
                              ),
                              color: Colors.blue,
                            ),
                            child: Text(
                              'Type',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ),
                          Container(
                            width: 150,
                            padding: EdgeInsets.symmetric(vertical: 5.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border(
                                bottom: BorderSide(width: 1),
                                left: BorderSide(width: 1),
                                right: BorderSide(width: 1),
                              ),
                            ),
                            child: Text(
                              widget.projectModel.projectType[0],
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20.0),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 30.0),
                      Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 5.0),
                            width: 150,
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border(
                                top: BorderSide(width: 1),
                                left: BorderSide(width: 1),
                                right: BorderSide(width: 1),
                              ),
                              color: Colors.green,
                            ),
                            child: Text(
                              'Status',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ),
                          Container(
                            width: 150,
                            padding: EdgeInsets.symmetric(vertical: 5.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border(
                                bottom: BorderSide(width: 1),
                                left: BorderSide(width: 1),
                                right: BorderSide(width: 1),
                              ),
                            ),
                            child: Text(
                              widget.projectModel.projectStatus,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ),
                          SizedBox(height: 30.0),
                          Container(
                            width: double.infinity,
                            child: RaisedButton(
                              color: Color.fromRGBO(27, 20, 100, 1),
                              onPressed: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return ModuleView(
                                      projectModel: widget.projectModel);
                                }));
                              },
                              child: Text('View Module', style: TextStyle(color: Colors.white),),
                            ),
                          ),
                          SizedBox(height: 10.0),
                          Builder(builder: (BuildContext context){
                            return Container(
                            width: double.infinity,
                            child: RaisedButton(
                              onPressed: () {
                                final snackBar = SnackBar(
                                  content: Text('Export is successfully'),
                                  action: SnackBarAction(label: 'Undo', onPressed: (){}),
                                );
                                Scaffold.of(context).showSnackBar(snackBar);
                              },
                              child: Text('Export to PDF'),
                            ),
                          );
                          })
                          
                        ],
                      )
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
