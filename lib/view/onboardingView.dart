import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:run_time_project/view/login_view.dart';

class OnboardingView extends StatefulWidget {
  @override
  _OnboardingViewState createState() => _OnboardingViewState();
}

class _OnboardingViewState extends State<OnboardingView> {
  final introKey = GlobalKey<IntroductionScreenState>();
  @override
  Widget build(BuildContext context) {
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(
          color: Colors.white, fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: TextStyle(color: Colors.white, fontSize: 19.0),
      pageColor: Color.fromRGBO(27, 20, 100, 1),
      imagePadding: EdgeInsets.zero,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
    );
    return IntroductionScreen(
      key: introKey,
      pages: [
        PageViewModel(
            image: Image.asset("images/onboarding_1.png"),
            title: "Accurate Time Tracking",
            body: "Track your working time to be more accurate",
            decoration: pageDecoration),
        PageViewModel(
            image: Image.asset("images/onboarding_2.png"),
            title: "Completeness & Accurate",
            body:
                "Track your working description to be more complete & accurate",
            decoration: pageDecoration),
        PageViewModel(
          image: Image.asset("images/onboarding_3.png"),
          title: "Time Tracking Report",
          body: "Easier to tracking & export daily report",
          footer: RaisedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LoginView()),
              );
            },
            child: Text("Get Started"),
            color: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
          ),
          decoration: pageDecoration,
        )
      ],
      onDone: () {},
      done: Text(
        "",
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
