import 'package:flutter/material.dart';
import 'package:run_time_project/model/group_model.dart';
import 'package:run_time_project/model/module_model.dart';
import 'package:run_time_project/model/modules_model.dart';
import 'package:run_time_project/model/project_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/view/assign_employee_to_group_view.dart';

class GroupDetailView extends StatefulWidget {
  GroupModel groupModel;

  GroupDetailView({this.groupModel});

  @override
  _GroupDetailViewViewState createState() => _GroupDetailViewViewState();
}

class _GroupDetailViewViewState extends State<GroupDetailView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Group Detail'),
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          _groupNameTextField(),
          _groupDescriptionTextField(),
          _assignButton(),
        ],
      ),
    );
  }

  Widget _groupNameTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.groupModel.groupName,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Group Name',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _groupDescriptionTextField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        enabled: false,
        initialValue: widget.groupModel.groupDescription,
        enableInteractiveSelection: true,
        keyboardType: TextInputType.emailAddress,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: 'Group Description',
        ),
        onChanged: (String value) async {},
      ),
    );
  }

  Widget _assignButton() {
    return Container(
      width: 250.0,
      height: 40,
      child: RaisedButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return AssignEmployeeToGroupView(groupModel: widget.groupModel);
          }));
        },
        color: Color.fromRGBO(27, 20, 100, 1),
        child: Text('Assign employee to group',
            style: TextStyle(color: Colors.white)),
      ),
    );
  }
}
