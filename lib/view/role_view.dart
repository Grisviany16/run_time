import 'package:flutter/material.dart';
import 'package:run_time_project/model/role_model.dart';
import 'package:run_time_project/model/roles_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/view/add_role_view.dart';

class RoleView extends StatefulWidget {
  @override
  _RoleViewState createState() => _RoleViewState();
}

class _RoleViewState extends State<RoleView> {
  Provider provider = Provider();

  Future<RolesModel> _getRoles() async {
    RolesModel rolesModel = await provider.getRoles();
    return rolesModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(27, 20, 100, 1),
          title: Text("Roles"),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddRoleView()),
            );
          },
        ),
        body: FutureBuilder<RolesModel>(
          future: _getRoles(),
          builder: (context, snapshot) {
            if (snapshot.data != null) {
              RolesModel rolesModel = snapshot.data;

              return ListView.builder(
                itemCount: rolesModel.roleModels.length,
                itemBuilder: (BuildContext context, int index) {
                  RoleModel roleModel = rolesModel.roleModels[index];
                  return _itemRole(roleModel);
                },
              );
            } else {
              return Text('No data');
            }
          },
        ));
  }

  Widget _itemRole(RoleModel roleModel) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(roleModel.roleName),
      ),
    );
  }
}
