import 'dart:async';

import 'package:flutter/material.dart';
import 'package:run_time_project/model/body/create_time_track.dart';
import 'package:run_time_project/model/project_model.dart';
import 'package:run_time_project/model/projects_model.dart';
import 'package:run_time_project/model/time_track_model.dart';
import 'package:run_time_project/model/time_tracks_model.dart';
import 'package:run_time_project/network/provider.dart';
import 'package:run_time_project/util/util.dart';
import 'package:run_time_project/view/notification_view.dart';
import 'package:run_time_project/view/timer_card_detail_view.dart';

class TimeTrackView extends StatefulWidget {
  @override
  _TimeTrackViewState createState() => _TimeTrackViewState();
}

class _TimeTrackViewState extends State<TimeTrackView> {
  final provider = Provider();
  String displayTotalTime = '';
  Stopwatch watch = Stopwatch();
  Timer timer;
  bool startStop = true;
  String elapsedTime = '00:00:00';
  final ValueNotifier<String> elapsedTimeValueNotifier =
      ValueNotifier('00:00:00');
  int selectedProjectModelIndex = 0;
  List<ProjectModel> projectModels;
  bool dontUpdateStartDateTime = false;
  DateTime startDateTime;

  updateTime(Timer timer) {
    if (watch.isRunning) {
      elapsedTime = transformMilliSeconds(watch.elapsedMilliseconds);
      elapsedTimeValueNotifier.value = elapsedTime;
      elapsedTimeValueNotifier.notifyListeners();
    }
  }

  startOrStop() {
    if (startStop) {
      startWatch();
    } else {
      stopWatch();
    }
  }

  startWatch() {
    setState(() {
      startStop = false;
      watch.start();
      timer = Timer.periodic(Duration(milliseconds: 100), updateTime);
      if (!dontUpdateStartDateTime) {
        dontUpdateStartDateTime = true;
        startDateTime = DateTime.now();
      }
    });
  }

  stopWatch() {
    setState(() {
      startStop = true;
      watch.stop();
      this.displayTotalTime = setTime();
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return TimerCardDetailView(
          createTimeTrack: CreateTimeTrack(
              '',
              '',
              startDateTime.millisecondsSinceEpoch,
              DateTime.now().millisecondsSinceEpoch,
              projectModels[selectedProjectModelIndex].sId),
        );
      }));
    });
  }

  setTime() {
    var timeSoFar = watch.elapsedMilliseconds;
    setState(() {
      elapsedTime = transformMilliSeconds(timeSoFar);
    });
  }

  transformMilliSeconds(int milliseconds) {
    int hundreds = (milliseconds / 10).truncate();
    int seconds = (hundreds / 100).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();

    String hoursStr = (hours % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    return "$hoursStr:$minutesStr:$secondsStr";
  }

  Future<TimeTracksModel> _getTimeTracks() async {
    TimeTracksModel timeTracksModel = await provider.getTimeTracks();
    return timeTracksModel;
  }

  Future<ProjectsModel> _getProjects() async {
    ProjectsModel projectsModel = await provider.getProjects();
    return projectsModel;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(27, 20, 100, 1),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NotificationView()),
                ),
                child: Icon(
                  Icons.notifications,
                  size: 26.0,
                ),
              )),
        ],
      ),
      body: Column(
        children: <Widget>[
          _welcomeWidget(),
          _timeCard(),
          _historyTitleWidget(),
          _taskList(),
        ],
      ),
    );
  }

  Widget _taskList() {
    return Expanded(
      child: FutureBuilder<TimeTracksModel>(
        future: _getTimeTracks(),
        builder: (context, snapshot) {
          if (snapshot.data != null) {
            if (snapshot.data.timeTrackModels.length == 0) {
              return Center(child: Text('No data'));
            } else {
              return ListView.builder(
                itemCount: snapshot.data.timeTrackModels.length,
                itemBuilder: (BuildContext context, int index) {
                  TimeTrackModel timeTrackModel =
                      snapshot.data.timeTrackModels[index];
                  return _itemTask(timeTrackModel);
                },
              );
            }
          } else {
            return Center(child: Text('No data'));
          }
        },
      ),
    );
  }

  Widget _itemTask(TimeTrackModel timeTrackModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(convertDateFromString(timeTrackModel.createdDate)),
        Text(timeTrackModel.timeDescription),
      ],
    );
  }

  Widget _timeCard() {
    return Container(
      color: Color.fromRGBO(27, 20, 100, 1),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            ValueListenableBuilder(
              valueListenable: elapsedTimeValueNotifier,
              builder:
                  (BuildContext context, String elapsedTime, Widget child) {
                return Text(
                  elapsedTime,
                  style: TextStyle(
                    fontSize: 40.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                );
              },
            ),
            FutureBuilder<ProjectsModel>(
              future: _getProjects(),
              builder: (context, snapshot) {
                if (snapshot.data != null) {
                  projectModels = snapshot.data.projectModels;
                  if (projectModels.length != 0) {
                    return DropdownButton(
                      value: projectModels[selectedProjectModelIndex],
                      hint: Text("Choose project"),
                      items: projectModels.map((ProjectModel value) {
                        return DropdownMenuItem(
                          value: value,
                          child: new Row(
                            children: <Widget>[new Text(value.projectName)],
                          ),
                        );
                      }).toList(),
                      onChanged: (ProjectModel value) {
                        setState(() {
                          selectedProjectModelIndex =
                              projectModels.indexOf(value);
                        });
                      },
                    );
                  } else {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('No data'),
                    );
                  }
                } else {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('No data'),
                  );
                }
              },
            ),
            Container(
              width: 200,
              height: 40,
              child: RaisedButton(
                color: Colors.blue,
                onPressed: () => startOrStop(),
                child: Text(startStop ? "START" : "STOP"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _historyTitleWidget() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(top: 20.0),
      padding: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.blueGrey[400],
        border: Border.all(width: 1),
      ),
      child: Text(
        "History",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _welcomeWidget() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 30, bottom: 30),
      color: Color.fromRGBO(27, 20, 100, 1),
      child: Text(
        'Welcome, Ricky! ',
        style: TextStyle(
            fontWeight: FontWeight.bold, color: Colors.white, fontSize: 25.0),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
