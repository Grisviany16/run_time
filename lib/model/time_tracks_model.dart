import 'package:run_time_project/model/base_response.dart';
import 'package:run_time_project/model/time_track_model.dart';

class TimeTracksModel extends BaseResponse {
  List<TimeTrackModel> timeTrackModels = List();

  TimeTracksModel(this.timeTrackModels);

  TimeTracksModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      for (int i = 0; i < json['data'].length; i++) {
        timeTrackModels.add(TimeTrackModel.fromJson(json['data'][i]));
      }
    }
    status = json['status'] != null ? json['status'] : '';
    message = json['message'] != null ? json['message'] : '';
    statusCode = json['statusCode'] != null ? json['statusCode'] : 0;
  }
}
