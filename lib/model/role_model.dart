class RoleModel {
  String sId;
  String updatedDate;
  String createdDate;
  String roleName;
  int iV;

  RoleModel(
      {this.sId, this.updatedDate, this.createdDate, this.roleName, this.iV});

  RoleModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    updatedDate = json['updatedDate'];
    createdDate = json['createdDate'];
    roleName = json['roleName'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['updatedDate'] = this.updatedDate;
    data['createdDate'] = this.createdDate;
    data['roleName'] = this.roleName;
    data['__v'] = this.iV;
    return data;
  }
}
