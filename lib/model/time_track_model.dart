import 'package:run_time_project/model/base_response.dart';
import 'package:run_time_project/model/module_model.dart';

class TimeTrackModel extends BaseResponse {
  bool isDeleted;
  String sId;
  String timeName;
  String timeDescription;
  int timeStart;
  int timeEnd;
  ModuleModel moduleModel;
  int timeTotal;
  String employee;
  String updatedDate;
  String createdDate;
  int iV;

  TimeTrackModel(
      {this.isDeleted,
      this.sId,
      this.timeName,
      this.timeDescription,
      this.timeStart,
      this.timeEnd,
      this.moduleModel,
      this.timeTotal,
      this.employee,
      this.updatedDate,
      this.createdDate,
      this.iV});

  TimeTrackModel.fromJson(Map<String, dynamic> json) {
    isDeleted = json['isDeleted'];
    sId = json['_id'];
    timeName = json['timeName'];
    timeDescription = json['timeDescription'];
    timeStart = json['timeStart'];
    timeEnd = json['timeEnd'];
    moduleModel = json['module'] != null
        ? new ModuleModel.fromJson(json['module'])
        : null;
    timeTotal = json['timeTotal'];
    employee = json['employee'];
    updatedDate = json['updatedDate'];
    createdDate = json['createdDate'];
    iV = json['__v'];

    status = json['status'] != null ? json['status'] : '';
    message = json['message'] != null ? json['message'] : '';
    statusCode = json['statusCode'] != null ? json['statusCode'] : 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isDeleted'] = this.isDeleted;
    data['_id'] = this.sId;
    data['timeName'] = this.timeName;
    data['timeDescription'] = this.timeDescription;
    data['timeStart'] = this.timeStart;
    data['timeEnd'] = this.timeEnd;
    if (this.moduleModel != null) {
      data['module'] = this.moduleModel.toJson();
    }
    data['timeTotal'] = this.timeTotal;
    data['employee'] = this.employee;
    data['updatedDate'] = this.updatedDate;
    data['createdDate'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}
