import 'package:run_time_project/model/base_response.dart';
import 'package:run_time_project/model/notification_model.dart';
import 'package:run_time_project/model/project_model.dart';

class NotificationsModel extends BaseResponse {
  List<NotificationModel> notificationModels = List();

  NotificationsModel(this.notificationModels);

  NotificationsModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      for (int i = 0; i < json['data'].length; i++) {
        notificationModels.add(NotificationModel.fromJson(json['data'][i]));
      }
    }
    status = json['status'] != null ? json['status'] : '';
    message = json['message'] != null ? json['message'] : '';
    statusCode = json['statusCode'] != null ? json['statusCode'] : 0;
  }
}
