class ProjectModel {
  List<String> projectType = List();
  String projectStatus;
  bool isDeleted;
  String sId;
  String group;
  String projectName;
  String projectDescription;
  int projectTotalHours;
  String updatedDate;
  String createdDate;
  int iV;

  ProjectModel(
      {this.projectType,
      this.projectStatus,
      this.isDeleted,
      this.sId,
      this.group,
      this.projectName,
      this.projectDescription,
      this.projectTotalHours,
      this.updatedDate,
      this.createdDate,
      this.iV});

  ProjectModel.fromJson(Map<String, dynamic> json) {
    for (int i = 0; i < json['projectType'].length; i++) {
      projectType.add(json['projectType'][i]);
    }
    projectStatus = json['projectStatus'];
    isDeleted = json['isDeleted'];
    sId = json['_id'];
    group = json['group'];
    projectName = json['projectName'];
    projectDescription = json['projectDescription'];
    projectTotalHours = json['projectTotalHours'];
    updatedDate = json['updatedDate'];
    createdDate = json['createdDate'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['projectType'] = this.projectType;
    data['projectStatus'] = this.projectStatus;
    data['isDeleted'] = this.isDeleted;
    data['_id'] = this.sId;
    data['group'] = this.group;
    data['projectName'] = this.projectName;
    data['projectDescription'] = this.projectDescription;
    data['projectTotalHours'] = this.projectTotalHours;
    data['updatedDate'] = this.updatedDate;
    data['createdDate'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}
