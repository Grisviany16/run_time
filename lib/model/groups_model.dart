import 'package:run_time_project/model/base_response.dart';

import 'group_model.dart';

class GroupsModel extends BaseResponse {
  List<GroupModel> groupModels;

  GroupsModel({this.groupModels});

  GroupsModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      groupModels = new List<GroupModel>();
      json['data'].forEach((v) {
        groupModels.add(new GroupModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.groupModels != null) {
      data['data'] = this.groupModels.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
