import 'package:run_time_project/model/base_response.dart';
import 'package:run_time_project/model/project_model.dart';
import 'package:run_time_project/model/role_model.dart';

class RolesModel extends BaseResponse {
  List<RoleModel> roleModels = List();

  RolesModel(this.roleModels);

  RolesModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      for (int i = 0; i < json['data'].length; i++) {
        roleModels.add(RoleModel.fromJson(json['data'][i]));
      }
    }
    status = json['status'] != null ? json['status'] : '';
    message = json['message'] != null ? json['message'] : '';
    statusCode = json['statusCode'] != null ? json['statusCode'] : 0;
  }
}
