import 'package:run_time_project/model/base_response.dart';
import 'package:run_time_project/model/project_model.dart';

class ModuleModel {
  String moduleStatus;
  bool isDeleted;
  String sId;
  ProjectModel projectModel;
  String moduleName;
  String moduleDescription;
  String modulePriority;
  String updatedDate;
  String createdDate;
  int iV;

  ModuleModel(
      {this.moduleStatus,
      this.isDeleted,
      this.sId,
      this.projectModel,
      this.moduleName,
      this.moduleDescription,
      this.modulePriority,
      this.updatedDate,
      this.createdDate,
      this.iV});

  ModuleModel.fromJson(Map<String, dynamic> json) {
    moduleStatus = json['moduleStatus'];
    isDeleted = json['isDeleted'];
    sId = json['_id'];
    projectModel = json['project'] != null
        ? new ProjectModel.fromJson(json['project'])
        : null;
    moduleName = json['moduleName'];
    moduleDescription = json['moduleDescription'];
    modulePriority = json['modulePriority'];
    updatedDate = json['updatedDate'];
    createdDate = json['createdDate'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['moduleStatus'] = this.moduleStatus;
    data['isDeleted'] = this.isDeleted;
    data['_id'] = this.sId;
    if (this.projectModel != null) {
      data['project'] = this.projectModel.toJson();
    }
    data['moduleName'] = this.moduleName;
    data['moduleDescription'] = this.moduleDescription;
    data['modulePriority'] = this.modulePriority;
    data['updatedDate'] = this.updatedDate;
    data['createdDate'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}
