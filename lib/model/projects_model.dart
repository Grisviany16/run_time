import 'package:run_time_project/model/base_response.dart';
import 'package:run_time_project/model/project_model.dart';

class ProjectsModel extends BaseResponse {
  List<ProjectModel> projectModels = List();

  ProjectsModel(this.projectModels);

  ProjectsModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      for (int i = 0; i < json['data'].length; i++) {
        projectModels.add(ProjectModel.fromJson(json['data'][i]));
      }
    }
    status = json['status'] != null ? json['status'] : '';
    message = json['message'] != null ? json['message'] : '';
    statusCode = json['statusCode'] != null ? json['statusCode'] : 0;
  }
}
