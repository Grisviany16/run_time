import 'package:run_time_project/model/base_response.dart';

import 'employee_model.dart';
import 'group_model.dart';

class EmployeesModel extends BaseResponse {
  List<EmployeeModel> employeeModels;

  EmployeesModel({this.employeeModels});

  EmployeesModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      employeeModels = new List<EmployeeModel>();
      json['data'].forEach((v) {
        employeeModels.add(new EmployeeModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.employeeModels != null) {
      data['data'] = this.employeeModels.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
