import 'package:run_time_project/model/base_response.dart';

import 'employee_model.dart';

class LoginModel extends BaseResponse {
  EmployeeModel employee;
  String token;

  LoginModel({this.employee, this.token});

  LoginModel.fromJson(Map<String, dynamic> json) {
    employee = json['data'] != null
        ? json['data']['employee'] != null
            ? new EmployeeModel.fromJson(json['data']['employee'])
            : null
        : null;
    token = json['data'] != null
        ? json['data']['token'] != null ? json['data']['token'] : null
        : null;
    status = json['status'] != null ? json['status'] : '';
    ;
    message = json['message'] != null ? json['message'] : '';
    statusCode = json['statusCode'] != null ? json['statusCode'] : 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.employee != null) {
      data['employee'] = this.employee.toJson();
    }
    data['token'] = this.token;
    return data;
  }
}
