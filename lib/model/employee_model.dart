import 'package:run_time_project/model/role_model.dart';

class EmployeeModel {
  bool employeeStatus;
  String sId;
  String updatedDate;
  String createdDate;
  RoleModel role;
  String employeeEmail;
  String employeePhoneNumber;
  String employeePassword;
  int iV;
  String employeeProfileStatus;
  String employeeFirstName;
  String employeeLastName;

  EmployeeModel(
      {this.employeeStatus,
      this.sId,
      this.updatedDate,
      this.createdDate,
      this.role,
      this.employeeEmail,
      this.employeePhoneNumber,
      this.employeePassword,
      this.iV,
      this.employeeProfileStatus,
      this.employeeFirstName,
      this.employeeLastName});

  EmployeeModel.fromJson(Map<String, dynamic> json) {
    employeeStatus = json['employeeStatus'];
    sId = json['_id'];
    updatedDate = json['updatedDate'];
    createdDate = json['createdDate'];
    role = json['role'] != null ? new RoleModel.fromJson(json['role']) : null;
    employeeEmail = json['employeeEmail'];
    employeePhoneNumber = json['employeePhoneNumber'];
    employeePassword = json['employeePassword'];
    iV = json['__v'];
    employeeProfileStatus = json['employeeProfileStatus'];
    employeeFirstName = json['employeeFirstName'];
    employeeLastName = json['employeeLastName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['employeeStatus'] = this.employeeStatus;
    data['_id'] = this.sId;
    data['updatedDate'] = this.updatedDate;
    data['createdDate'] = this.createdDate;
    data['role'] = this.role;
    data['employeeEmail'] = this.employeeEmail;
    data['employeePhoneNumber'] = this.employeePhoneNumber;
    data['employeePassword'] = this.employeePassword;
    data['__v'] = this.iV;
    data['employeeProfileStatus'] = this.employeeProfileStatus;
    data['employeeFirstName'] = this.employeeFirstName;
    data['employeeLastName'] = this.employeeLastName;
    return data;
  }
}
