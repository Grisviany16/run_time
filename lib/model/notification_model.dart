class NotificationModel {
  String notificationStatus;
  String sId;
  String updatedDate;
  String createdDate;
  String employee;
  String notificationName;
  String notificationDescription;
  int iV;

  NotificationModel(
      {this.notificationStatus,
      this.sId,
      this.updatedDate,
      this.createdDate,
      this.employee,
      this.notificationName,
      this.notificationDescription,
      this.iV});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    notificationStatus = json['notificationStatus'];
    sId = json['_id'];
    updatedDate = json['updatedDate'];
    createdDate = json['createdDate'];
    employee = json['employee'];
    notificationName = json['notificationName'];
    notificationDescription = json['notificationDescription'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notificationStatus'] = this.notificationStatus;
    data['_id'] = this.sId;
    data['updatedDate'] = this.updatedDate;
    data['createdDate'] = this.createdDate;
    data['employee'] = this.employee;
    data['notificationName'] = this.notificationName;
    data['notificationDescription'] = this.notificationDescription;
    data['__v'] = this.iV;
    return data;
  }
}
