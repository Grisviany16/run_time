class GroupModel {
  bool isActive;
  bool isDeleted;
  String sId;
  String groupName;
  String groupDescription;
  String updatedDate;
  String createdDate;
  int iV;

  GroupModel({
    this.isActive,
    this.isDeleted,
    this.sId,
    this.groupName,
    this.groupDescription,
    this.updatedDate,
    this.createdDate,
    this.iV,
  });

  GroupModel.fromJson(Map<String, dynamic> json) {
    isActive = json['isActive'];
    isDeleted = json['isDeleted'];
    sId = json['_id'];
    groupName = json['groupName'];
    groupDescription = json['groupDescription'];
    updatedDate = json['updatedDate'];
    createdDate = json['createdDate'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isActive'] = this.isActive;
    data['isDeleted'] = this.isDeleted;
    data['_id'] = this.sId;
    data['groupName'] = this.groupName;
    data['groupDescription'] = this.groupDescription;
    data['updatedDate'] = this.updatedDate;
    data['createdDate'] = this.createdDate;
    data['__v'] = this.iV;
    return data;
  }
}
