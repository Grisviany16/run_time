class ForgotPassword {
  String email;

  ForgotPassword(this.email);

  Map<String, dynamic> toJson() {
    // ignore: unnecessary_cast
    return {
      'email': this.email,
    } as Map<String, dynamic>;
  }
}
