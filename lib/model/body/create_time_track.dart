class CreateTimeTrack {
  String timeName;
  String timeDescription;
  int timeStart;
  int timeEnd;
  String module;

  CreateTimeTrack(this.timeName, this.timeDescription, this.timeStart,
      this.timeEnd, this.module);

  Map<String, dynamic> toJson() {
    return {
      'timeName': this.timeName,
      'timeDescription': this.timeDescription,
      'timeStart': this.timeStart,
      'timeEnd': this.timeEnd,
      'module': this.module,
    };
  }
}
