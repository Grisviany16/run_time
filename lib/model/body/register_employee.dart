class RegisterEmployee {
  String role;
  String firstName;
  String lastName;
  String email;
  String phoneNumber;
  String status;
  String password;

  RegisterEmployee(this.role, this.firstName, this.lastName, this.email,
      this.phoneNumber, this.status, this.password);

  Map<String, dynamic> toJson() {
    return {
      'role': this.role,
      'firstName': this.firstName,
      'lastName': this.lastName,
      'email': this.email,
      'phoneNumber': this.phoneNumber,
      'status': this.status,
      'password': this.password,
    };
  }
}
