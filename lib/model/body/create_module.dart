class CreateModule {
  String project;
  String moduleName;
  String moduleDescription;
  String modulePriority;

  CreateModule(this.project, this.moduleName, this.moduleDescription,
      this.modulePriority);

  Map<String, dynamic> toJson() {
    return {
      'project': this.project,
      'moduleName': this.moduleName,
      'moduleDescription': this.moduleDescription,
      'modulePriority': this.modulePriority,
    };
  }
}
