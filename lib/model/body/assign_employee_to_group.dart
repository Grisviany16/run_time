class AssignEmployeeToGroup {
  String groupId;
  String employee;
  bool isLeader;

  AssignEmployeeToGroup(this.groupId, this.employee, this.isLeader);

  Map<String, dynamic> toJson() {
    return {
      'groupId': this.groupId,
      'employee': this.employee,
      'isLeader': this.isLeader,
    };
  }
}
