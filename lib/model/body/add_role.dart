class AddRole {
  String roleName;

  AddRole({
    this.roleName,
  });

  Map<String, dynamic> toJson() {
    // ignore: unnecessary_cast
    return {
      'roleName': this.roleName,
    } as Map<String, dynamic>;
  }
}
