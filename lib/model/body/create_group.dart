class CreateGroup {
  String groupName;
  String groupDescription;

  CreateGroup(this.groupName, this.groupDescription);

  Map<String, dynamic> toJson() {
    // ignore: unnecessary_cast
    return {
      'groupName': this.groupName,
      'groupDescription': this.groupDescription,
    };
  }
}
