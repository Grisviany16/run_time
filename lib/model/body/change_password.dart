class ChangePasword {
  String oldPassword;
  String newPassword;

  ChangePasword(this.oldPassword, this.newPassword);

  Map<String, dynamic> toJson() {
    // ignore: unnecessary_cast
    return {
      'oldPassword': this.oldPassword,
      'newPassword': this.newPassword,
    } as Map<String, dynamic>;
  }
}
