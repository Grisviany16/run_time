import 'package:run_time_project/model/base_response.dart';

class FakeModel extends BaseResponse {
  FakeModel();

  FakeModel.fromJson(Map<String, dynamic> json) {
    status = json['status'] != null ? json['status'] : '';
    message = json['message'] != null ? json['message'] : '';
    statusCode = json['statusCode'] != null ? json['statusCode'] : 0;
  }
}
