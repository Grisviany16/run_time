import 'package:run_time_project/model/base_response.dart';
import 'package:run_time_project/model/project_model.dart';

import 'module_model.dart';

class ModulesModel extends BaseResponse {
  List<ModuleModel> moduleModels = List();

  ModulesModel(this.moduleModels);

  ModulesModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      for (int i = 0; i < json['data'].length; i++) {
        moduleModels.add(ModuleModel.fromJson(json['data'][i]));
      }
    }
    status = json['status'] != null ? json['status'] : '';
    message = json['message'] != null ? json['message'] : '';
    statusCode = json['statusCode'] != null ? json['statusCode'] : 0;
  }
}
