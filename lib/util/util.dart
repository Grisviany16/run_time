import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:run_time_project/view/forgot_password_view.dart';
import 'package:run_time_project/view/login_view.dart';
import 'package:run_time_project/view/onboardingView.dart';
import 'package:run_time_project/view/splash_screen_view.dart';
import 'package:run_time_project/view/timer_track_view.dart';
import 'package:intl/intl.dart';

final routes = <String, WidgetBuilder>{
  '/login': (BuildContext context) => new LoginView(),
  '/home': (BuildContext context) => new TimeTrackView(),
  '/intro': (BuildContext context) => new OnboardingView(),
  '/forgotPass': (BuildContext context) => new ForgotPasswordView(),
  '/': (BuildContext context) => new SplashScreenView(),
};

String convertDateFromString(String strDate) {
  DateTime dateTime = DateTime.parse(strDate);
  DateFormat template = DateFormat('dd-MM-yyyy');
  return template.format(dateTime);
}

String convertDateFromMilis(int milis) {
  DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(milis);
  DateFormat template = DateFormat('dd-MM-yyyy HH:mm');
  return template.format(dateTime);
}

void showToast(String message, BuildContext context) {
  FlutterToast(context).showToast(
    child: Container(
      height: 50.0,
      color: Colors.black,
      child: Center(
        child: Text(
          message,
          style: TextStyle(color: Colors.white),
        ),
      ),
    ),
  );
}
