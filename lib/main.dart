import 'package:flutter/material.dart';
import 'package:run_time_project/view/dashboard_view.dart';
import 'package:run_time_project/view/login_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Runtime',
      home: FutureBuilder<SharedPreferences>(
        future: SharedPreferences.getInstance(),
        builder: (context, snapshot) {
          if (snapshot.data != null) {
            String token = snapshot.data.get('token');
            if (token == null) {
              return LoginView();
            } else {
              if (token.isEmpty) {
                return LoginView();
              } else {
                return DashboardView();
              }
            }
          } else {
            return LoginView();
          }
//          return DashboardView();
        },
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
