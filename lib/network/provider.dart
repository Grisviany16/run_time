import 'dart:collection';

import 'package:run_time_project/model/body/add_role.dart';
import 'package:run_time_project/model/body/change_password.dart';
import 'package:run_time_project/model/body/create_group.dart';
import 'package:run_time_project/model/body/forgot_password.dart';
import 'package:run_time_project/model/fake_model.dart';
import 'package:run_time_project/model/body/assign_employee_to_group.dart';
import 'package:run_time_project/model/body/create_module.dart';
import 'package:run_time_project/model/body/create_time_track.dart';
import 'package:run_time_project/model/body/register_employee.dart';
import 'package:run_time_project/model/employees_model.dart';
import 'package:run_time_project/model/groups_model.dart';
import 'package:run_time_project/model/login_model.dart';
import 'package:run_time_project/model/modules_model.dart';
import 'package:run_time_project/model/notifications_model.dart';
import 'package:run_time_project/model/projects_model.dart';
import 'package:run_time_project/model/roles_model.dart';
import 'package:run_time_project/model/time_track_model.dart';
import 'package:run_time_project/model/time_tracks_model.dart';
import 'package:run_time_project/network/dio_manager.dart';

class Provider {
  Future<LoginModel> login(Map<String, dynamic> body) async {
    try {
      var resp = await DioManager.getDio().post('auth/login', data: body);

      var data = LoginModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<TimeTracksModel> getTimeTracks() async {
    try {
      var resp = await DioManager.getDio().get('time-tracks');

      TimeTracksModel data = TimeTracksModel.fromJson(resp.data);

      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<TimeTrackModel> createTimeTrack(
      CreateTimeTrack createTimeTrack) async {
    try {
      var resp = await DioManager.getDio()
          .post('time-tracks', data: createTimeTrack.toJson());

      TimeTrackModel data = TimeTrackModel.fromJson(resp.data);

      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<ProjectsModel> getProjects() async {
    try {
      var resp = await DioManager.getDio().get('projects');

      var data = ProjectsModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<ModulesModel> getModulesByProject(String module) async {
    try {
      var resp = await DioManager.getDio().get('projects/${module}/modules');

      var data = ModulesModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<FakeModel> createModule(CreateModule createModule) async {
    try {
      var resp = await DioManager.getDio()
          .post('modules', data: createModule.toJson());

      var data = FakeModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<GroupsModel> getGroups() async {
    try {
      var resp = await DioManager.getDio().get('groups');

      var data = GroupsModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<FakeModel> createGroup(CreateGroup createGroup) async {
    try {
      var resp =
          await DioManager.getDio().post('groups', data: createGroup.toJson());

      var data = FakeModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<EmployeesModel> getEmployees() async {
    try {
      var resp = await DioManager.getDio().get('employees');

      var data = EmployeesModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<FakeModel> assignEmployeeToGroup(
      AssignEmployeeToGroup assignEmployeeToGroup) async {
    try {
      var resp = await DioManager.getDio().post(
          'groups/${assignEmployeeToGroup.groupId}/employees',
          data: assignEmployeeToGroup.toJson());

      var data = FakeModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<FakeModel> registerEmployee(RegisterEmployee registerEmployee) async {
    try {
      var resp = await DioManager.getDio()
          .post('auth/register-employee', data: registerEmployee.toJson());

      var data = FakeModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  // Notification
  Future<NotificationsModel> getNotifications() async {
    try {
      var resp = await DioManager.getDio().get('notifications');

      var data = NotificationsModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<RolesModel> getRoles() async {
    try {
      var resp = await DioManager.getDio().get('roles');

      var data = RolesModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<FakeModel> addRole(AddRole addRole) async {
    try {
      var resp = await DioManager.getDio()
          .post('roles/create-role', data: addRole.toJson());

      var data = FakeModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<FakeModel> changePassword(ChangePasword changePasword) async {
    try {
      var resp = await DioManager.getDio()
          .post('employees/change-password', data: changePasword.toJson());

      var data = FakeModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }

  Future<FakeModel> forgotPassword(ForgotPassword forgotPassword) async {
    try {
      var resp = await DioManager.getDio()
          .post('auth/forgot-password', data: forgotPassword.toJson());

      var data = FakeModel.fromJson(resp.data);
      return data;
    } catch (error, stacktrace) {
      print("Provider error ${error.toString()} ${stacktrace.toString()}");
      return null;
    }
  }
}
