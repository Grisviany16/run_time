import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DioManager {
  Dio dio;

  static DioManager _instance;

  DioManager._internal() {
    dio = new Dio();
    BaseOptions option = dio.options;

    dio.interceptors.add(DioInterceptors());

    option.baseUrl = 'http://054997a94e08.ngrok.io/api/';
    option.connectTimeout = 10000;
    option.receiveTimeout = 10000;
    dio.options.contentType = Headers.formUrlEncodedContentType;
  }

  static DioManager getInstance() {
    if (_instance == null) {
      _instance = DioManager._internal();
    }

    return _instance;
  }

  static Dio getDio() {
    return getInstance().dio;
  }
}

class DioInterceptors extends InterceptorsWrapper {
  @override
  Future onRequest(RequestOptions options) async {
    print(
        "DioManager request [${options?.method}] => path: ${options?.baseUrl}${options?.path}");

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    print('Token ${sharedPreferences.get('token')}');

    options.headers["token"] = sharedPreferences.get('token');

    return super.onRequest(options);
  }

  @override
  Future onResponse(Response response) {
    print(
        "DioManager response [${response?.statusCode}] => path: ${response?.request?.baseUrl}${response?.request?.path}");
    print("DioManager response => data: ${response?.data}");
    return super.onResponse(response);
  }

  @override
  Future onError(DioError err) {
    print(
        "DioManager error [${err?.response?.statusCode}] => path: ${err?.response?.request?.baseUrl}${err?.response?.request?.path}");
    print("DioManager error => cause: ${err?.error}");
    return super.onError(err);
  }
}
